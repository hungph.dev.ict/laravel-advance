<footer class="section section-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h4>
                    {{ Html::image('images/footer-logo.jpg') }}
                </h4>
            </div>
            <div class="col-sm-8">&nbsp;
                <h5 class="text-muted text-right">{{ __('Copyright (C) 2016 CREANSMAERD All Right Reserved.') }}</h5>
            </div>
        </div>
    </div>
</footer>
