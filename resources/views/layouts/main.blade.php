<html>
    <head>
        <meta charset="utf-8">
        <title>{{ __('Next front system') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{ Html::script('http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js') }}
        {{ Html::script('http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js') }}
        {{ Html::style('http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css') }}
        {{ Html::style('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css') }}
        {{ Html::style('stylesheets/default.css') }}
        @yield('inline_styles')
    </head>
    <body> 
        <!--Header-->
            @include('layouts.header')
        <!--./Header-->
        <div class="section main">
            <div class="container">
                <!--Title-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-header">
                            <h2>
                                @yield('title')
                            </h2>
                        </div>
                    </div>
                </div>
                <!--./Title-->
                <!--Message-->
                @include('flash::message')
                <!--/.Message-->
                <!--Content-->
                @yield('content')
                <!--./Content -->
            </div>
        </div>
        <!--Footer-->
            @include('layouts.footer')
        <!--./Footer-->
        <!--Script-->
            @yield('inline_scripts')
        <!--./Script-->
    </body>
</html>