@extends('layouts.main')
@section('title', __('Quản lý người dùng'))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="btn-link panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">{{ __('▼　Danh sách') }}</h3>
            </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{
                Form::open(['class' => 'form-horizontal',
                            'role' => 'form',
                            'method' => 'get'
                            ])
            }}
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="input-group">
                        {{
                            Form::text('key_search', '', ['class' => 'form-control'])
                        }}
                        <span class="input-group-btn">
                        {{
                            Form::submit( __('Tìm kiếm'), ['class' => 'btn btn-default'])
                        }}
                        </span>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 text-left">
            <p class="pagination">
                {{
                    Html::link(route('users.create'), __('Thêm mới'), ['class' => 'btn btn-default'])
                }}
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>{{ __('STT') }}</th>
                        <th>{{ __('Tên') }}</th>
                        <th>{{ __('Địa chỉ email') }}</th>
                        <th>{{ __('Vai trò') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $key => $user)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role }}</td>
                        <td>
                            <p class="text-right">
                                {{ Html::link(route('users.edit', $user), __('Sửa')) }}
                            </p>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 text-left">
            <p class="pagination">
                {{
                    Html::link(route('users.create'), __('Thêm mới'), ['class' => 'btn btn-default'])
                }}
            </p>
        </div>
    </div>
@stop