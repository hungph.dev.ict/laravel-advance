<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller as AppController;

class UsersController extends AppController
{
    /**
     * Constructor
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * List user
     *
     * /users
     */
    public function index()
    {
        // paginate user with condition
        $users = \App\User::all();

        return view('users.index', compact('users'));
    }

    /**
     * Create new user
     *
     * GET users/create
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Edit user
     *
     * GET users/{id}/edit
     *
     * @param \App\User $user
     */
    public function edit(\App\User $user)
    {
        return view('users.edit', compact(
            'user'
        ));
    }
}
